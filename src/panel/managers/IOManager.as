package panel.managers 
{
	import flash.utils.Dictionary;
	import panel.misc.Config;
	import panel.io.ServerAPI;
	import panel.misc.Strings;
	import panel.SvoeRadioPanel;
	import vk.api.serialization.json.JSON;
	import vk.APIConnection;
	/**
	 * Управляет загрузками.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	
	public class IOManager
	{
		private static const METHOD_EXECUTE:String = 'execute';
		private static const METHOD_FIRSTPANEL:String = 'common.firstPanel';
		private static const METHOD_AUDIO_GETBYID:String = 'audio.getById';
		private static const METHOD_GET_BROADCAST:String = 'queues.getBroadcast';
		private static const METHOD_GET_ALBUMS:String = 'audio.getAlbums';
		private static const METHOD_STATIONS_EDIT:String = 'stations.edit';
		private static const METHOD_QUEUES_SAVE:String = 'queues.save';
		private static const METHOD_STATIONS_SET_RUNNED:String = 'stations.setRunned';
		private static const METHOD_STATIONS_REMOVE:String = 'stations.remove';
		private static const METHOD_STATS_GET:String = 'stats.get';
		private static const METHOD_STATIONS_FREEZY:String = 'stations.freezy';
		private static const METHOD_STATIONS_DEFREEZY:String = 'stations.defreezy';
		
		private static var VK:APIConnection;
		private static var server:ServerAPI;
		
		private static var station_id:int;
		
		public static function init():void 
		{
			var flashVars:Object = SvoeRadioPanel.flashVars;
			VK = new APIConnection(flashVars);
			server = new ServerAPI(flashVars);
			VK.forceDirectApiAccess();
		}
		
		public static function firstLoad():void
		{
			VK.api(METHOD_EXECUTE, { code: Config.FIRST_VK_QUERY }, function(api_result:Object):void {
				var groups:Array = new Array(),
					temp:Array = api_result.groups as Array,
					group_id:int = SvoeRadioPanel.flashVars.group_id,
					group_index:int = -1,
					i:int;
				for (i = 1; i < temp.length; i++) {
					if (temp[i].gid == group_id)
						group_index = (i - 1);
					groups.push({ data:temp[i].gid, label:temp[i].name });
				}
				api_result.groups = groups;
				api_result.group_index = group_index;
				
				server.api(METHOD_FIRSTPANEL, {group_id:group_id}, function (result:Object):void {
					api_result.genres = result.genres;
					loadTracksNames(result.queue);
					api_result.params = result.params;
					station_id = api_result.params.id;
					
					temp = api_result.genres as Array;
					api_result.genres = new Array();
					api_result.genre_index = -1;
					for (i = 0; i < temp.length; i++) {
						if (temp[i].id == api_result.params.genre_id)
							api_result.genre_index = i;
						api_result.genres.push({ label:temp[i].name, data:temp[i].id });
					}
					getBroadcast();
					UIManager.firstLoad(api_result);
				}, onErrorHandler);
			}, onVKErrorHandler);
		}
		
		private static function loadTracksNames(tracks:Array):void
		{
			if (tracks.length == 0)
				return;
				
			var audios:Array = new Array();
			var i:int = 0;
			for (i = 0; i < tracks.length; i++) {
				audios.push(tracks[i].audio_id);
			}
			var temp:Dictionary = new Dictionary();
			
			VK.api(METHOD_AUDIO_GETBYID, { audios:audios.join(Strings.COMMA) }, function (result:Object):void {
				if ((result as Array).length == 0) result = new Object();
				var arr:Array = result as Array;
				for (i = 0; i < arr.length; i++)
					temp[arr[i].owner_id + Strings.DOWN_SPACE + arr[i].aid] = arr[i].artist + Strings.HYPHEN + arr[i].title;
				for (i = 0; i < tracks.length; i++) {
					var name:String = temp[tracks[i].audio_id];
					tracks[i].width = convertToHHMMSS(tracks[i].duration);
					if (name != null) tracks[i].name = name;
					else tracks[i].name = Strings.REMOVED_TRACK;
				}
				UIManager.setQueueTracks(tracks);
			}, onVKErrorHandler);
		}
		
		public static function getBroadcast():void
		{
			server.api(METHOD_GET_BROADCAST, { station_id:station_id }, function (result:Object):void {
				VK.api(METHOD_AUDIO_GETBYID, { audios:result.audio_id }, function (result2:Object):void {
					if (result2.length == 0) {
						UIManager.addStatusMessage("Невозможно воспроизвести удаленную станцию.");
						return;
					}
					result2 = result2[0];
					result2.audio_id = result.audio_id;
					result2.time = result.time * 1000;
					result2.duration *= 1000;
					result2.name = result2.artist + Strings.HYPHEN + result2.title;
					UIManager.setPlayingTrack(result2);
					AudioManager.play(result2);
				}, onVKErrorHandler);
			}, onErrorHandler);
		}
		
		public static function getStats(year:int, month:int):void
		{
			trace("stats.get", year, month);
			server.api(METHOD_STATS_GET, { station_id:station_id, year:year, month:month }, function(result:Object):void {
				UIManager.setStats(result as Array);
			}, onErrorHandler);
		}
		
		public static function getAlbumsByGroupId(group_id:int):void
		{
			VK.api(METHOD_GET_ALBUMS, { owner_id: -group_id }, function (result:Object):void {
				var albums:Array = new Array();
				var temp:Array = result as Array;
				albums.push( { label:Strings.ALL_ALBUMS, data:0 } );
				for (var i:int = 1; i < temp.length; i++) {
					albums.push({ data:temp[i].album_id, label:temp[i].title });
				}
				UIManager.setAlbums(albums);
			},function (error:Object):void {
				if (error.error_code == 15) onErrorHandler(error);
				else onVKErrorHandler(error);
			});
		}
		
		public static function search(method:String, params:Object):void
		{
			params.count = Config.SEARCH_COUNT;
			VK.api(method, params, function (result:Object):void {
				var tracks:Array = new Array();
				var temp:Array = result as Array;
				for (var i:int = 1; i < temp.length; i++) {
					tracks.push({ name:temp[i].artist + Strings.HYPHEN + temp[i].title, width:convertToHHMMSS(temp[i].duration), duration:temp[i].duration, audio_id:temp[i].owner_id + Strings.DOWN_SPACE + temp[i].aid });
				}
				UIManager.setSearchTracks(tracks);
			}, function (error:Object):void {
				if (error.error_code == 15) onErrorHandler(error);
				else onVKErrorHandler(error);
			});
		}
		
		public static function saveSettings(name:String, genre_id:int, group_id:int):void
		{
			server.api(METHOD_STATIONS_EDIT, { station_id:station_id, name:name, genre_id:genre_id, group_id:group_id }, function (result:Object):void {
				UIManager.showNotify(Strings.SETTINGS_SAVED);
				UIManager.hideLoading();
				UIManager.updateStationLink();
			}, onErrorHandler);
		}
		
		public static function saveQueue(tracks:Array):void
		{
			trace(JSON.encode(tracks));
			server.api(METHOD_QUEUES_SAVE, { station_id:station_id, tracks:JSON.encode(tracks) }, function (result:Object):void {
				UIManager.hideLoading();
				UIManager.showNotify(Strings.QUEUE_SAVED);
				UIManager.queueSaveBtnDisable();
			}, onErrorHandler);
		}
		
		public static function setRunned(state:int = 1):void
		{
			server.api(METHOD_STATIONS_SET_RUNNED, { station_id:station_id, runned:state }, function (result:Object):void {
				if (result == 1) {
					UIManager.playFirstQueueTrack();
					getBroadcast();
				} else {
					AudioManager.stop();
					UIManager.hideLoading();
				}
			}, onErrorHandler);
		}
		
		public static function freezy(state:int = 1):void
		{
			var method:String = (state == 1)?METHOD_STATIONS_FREEZY:METHOD_STATIONS_DEFREEZY;
			server.api(method, { station_id:station_id}, function (result:Object):void {
				if (result == 1)
					UIManager.addStatusMessage("Станция успешно " + ((state == 1)?"заморожена.":"разморожена."));
				else
					UIManager.addStatusMessage("Не удалось " + ((state == 1)?"заморозить":"разморозить") + "станцию.");
			}, onErrorHandler);
		}
		
		public static function removeStation():void
		{
			server.api(METHOD_STATIONS_REMOVE, { station_id:station_id }, function (result:Object):void {
				UIManager.addCriticalMessage(Strings.STATION_REMOVED);
			}, onErrorHandler);
		}
		
		private static function onVKErrorHandler(result:Object):void {
			var data:Object = { error_code:UIManager.ERROR_CODE_PROVIDER };
			onErrorHandler(data);
		}
		
		private static function onErrorHandler(error:Object):void {
			UIManager.onErrorHandler(error);
		}
		
		private static function convertToHHMMSS($seconds:Number):String
		{
			var s:Number = $seconds % 60;
			var m:Number = Math.floor(($seconds % 3600 ) / 60);
			var h:Number = Math.floor($seconds / (60 * 60));
			
			var hourStr:String = (h == 0) ? Strings.EMPT_STR : doubleDigitFormat(h) + Strings.COLON;
			var minuteStr:String = doubleDigitFormat(m) + Strings.COLON;
			var secondsStr:String = doubleDigitFormat(s);
			
			return hourStr + minuteStr + secondsStr;
		}
		
		private static function doubleDigitFormat($num:uint):String
		{
			if ($num < 10)
			{
				return (Strings.ZERO + $num);
			}
			return String($num);
		}
		
	}

}