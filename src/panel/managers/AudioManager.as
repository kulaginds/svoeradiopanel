package panel.managers 
{
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	import panel.misc.Config;
	import panel.misc.Strings;
	
	/**
	 * Управляет аудиопотоком.
	 * @author Dmitry Kulagin <kulaginds.pp.ua>
	 */
	
	public class AudioManager
	{
		private static var time:int; // для буферизации
		private static var buffering:Boolean; // для буферизации
		private static var buff_time:Number; // для буферизации
		private static var sound:Sound;
		private static var sound_transform:SoundTransform;
		private static var sound_channel:SoundChannel;
		private static var broadcast_timer:Timer;
		
		public static function init():void
		{
			sound_transform = new SoundTransform(Config.DEFAULT_VOLUME);
			sound_channel = new SoundChannel();
			broadcast_timer = new Timer(1000, 1);
			broadcast_timer.addEventListener(TimerEvent.TIMER_COMPLETE, function(e:TimerEvent):void {
				IOManager.getBroadcast();
			});
		}
		
		public static function set volume(value:Number):void
		{
			sound_transform.volume = value;
			sound_channel.soundTransform = sound_transform;
		}
		
		public static function stop():void
		{
			if (sound_channel != null) {
				sound_channel.stop();
			}
			
			if (sound != null) {
				try {
					sound.close();
				} catch (e:IOError) {}
			}
			broadcast_timer.stop();
		}
		
		public static function play(track:Object):void
		{
			if (sound_channel != null) {
				sound_channel.stop();
			}
			
			if (sound != null) {
				try {
					sound.close();
				} catch (e:IOError) {}
			}
			
			time = track.time;
			buffering = true;
			buff_time = new Date().time;
			
			sound = new Sound(new URLRequest(track.url));
			sound.addEventListener(IOErrorEvent.IO_ERROR, function(e:IOErrorEvent):void {
				var error:Object = {
					error_code:UIManager.ERROR_CODE_NOTICE,
					error_msg:Strings.AUDIO_IOERROR
				};
				UIManager.onErrorHandler(error);
			});
			sound.addEventListener(ProgressEvent.PROGRESS, onProgressHandler);
			sound_channel = sound.play(time, 0, sound_transform);
			sound_channel.addEventListener(Event.SOUND_COMPLETE, onSoundCompleteHandler);
			
			broadcast_timer.stop();
			broadcast_timer.delay = track.duration - track.time;
			broadcast_timer.start();
		}
		
		private static function onProgressHandler(e:ProgressEvent):void
		{
			if ((sound.length >= time) && buffering)
			{
				sound.removeEventListener(ProgressEvent.PROGRESS, onProgressHandler);
				sound_channel.stop();
				sound_channel = sound.play(time, 0, sound_transform);
				sound_channel.addEventListener(Event.SOUND_COMPLETE, onSoundCompleteHandler);
				time = 0;
				buffering = false;
				UIManager.setBroadcastStatus(Strings.IN_AIR);
			}
		}
		
		private static function onSoundCompleteHandler(e:Event):void
		{
			UIManager.clearQueueStrings();
		}
	}
}