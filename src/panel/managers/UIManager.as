package panel.managers 
{
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.core.mx_internal;
	import mx.managers.PopUpManager;
	import panel.misc.Config;
	import panel.misc.Strings;
	import panel.SvoeRadioPanel;
	
	/**
	 * Управляет графикой.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	 
	public class UIManager
	{
		private static const METHOD_AUDIO_SEARCH:String = 'audio.search';
		private static const METHOD_AUDIO_GET:String    = 'audio.get';
		
		public static const ERROR_CODE_NOTICE:int = 100;
		public static const ERROR_CODE_API:int = 300;
		public static const ERROR_CODE_CRITICAL:int = 200;
		public static const ERROR_CODE_PROVIDER:int = 400;
		
		private static var loading_window:Alert;
		
		public static function init():void 
		{
			if (SvoeRadioPanel.flashVars.group_id > 0) {
				loading();
			} else 
				addCriticalMessage(Strings.SELECT_STATION_IN_APP);
		}
		
		public static function addStatusMessage(msg:String):void {
			hideLoading();
			Alert.show(msg, Strings.ERROR);
		}
		
		public static function addCriticalMessage(msg:String):void {
			hideLoading();
			var al:Alert = Alert.show(msg, Strings.CRITICAL_ERROR, 0);
			al.mx_internal::alertForm.removeChild(al.mx_internal::alertForm.mx_internal::buttons[0]);
		}
		
		public static function loading():void
		{
			if (loading_window != null) return;
			loading_window = Alert.show(Strings.LOADING_WAIT_FOR, Strings.NOTICE, 0);
			loading_window.mx_internal::alertForm.removeChild(loading_window.mx_internal::alertForm.mx_internal::buttons[0]);
		}
		
		public static function hideLoading():void
		{
			if (loading_window == null) return;
			
			PopUpManager.removePopUp(loading_window);
			loading_window = null;
		}
		
		public static function showNotify(msg:String):void
		{
			Alert.show(msg, Strings.NOTICE);
		}
		
		public static function updateStationInfo(name:String, genre_id:int, group_id:int):void 
		{
			loading();
			IOManager.saveSettings(name, genre_id, group_id);
		}
		
		public static function updateStationLink():void
		{
			SvoeRadioPanel.instance().station_link = Config.STATION_LINK + SvoeRadioPanel.instance().station_group;
		}
		
		public static function removeStation():void 
		{
			IOManager.removeStation();
		}
		
		public static function play():void 
		{
			clearQueueStrings();
			AudioManager.stop();
			loading();
			IOManager.setRunned();
		}
		
		public static function stop():void 
		{
			clearQueueStrings();
			AudioManager.stop();
			IOManager.setRunned(0);
		}
		
		public static function clearQueueStrings():void
		{
			setBroadcastStatus(Strings.STOPING);
			setBroadcastTrack(Strings.EMPT_STR);
		}
		
		public static function playFirstQueueTrack():void
		{
			if (SvoeRadioPanel.instance().queue_tracks_dp.length > 0)
				SvoeRadioPanel.instance().queue_tracks_dp.removeItemAt(0);
		}
		
		public static function setVolume(value:Number):void 
		{
			AudioManager.volume = value;
		}
		
		public static function saveQueueTracks(data:Array):void 
		{
			loading();
			IOManager.saveQueue(data);
		}
		
		public static function queueSaveBtnDisable():void 
		{
			SvoeRadioPanel.instance().queue_save_btn.enabled = false;
		}
		
		public static function loadAlbums(group_id:int):void 
		{
			IOManager.getAlbumsByGroupId(group_id);
		}
		
		public static function setAlbums(albums:Array):void 
		{
			SvoeRadioPanel.instance().albums_dp.addAll(new ArrayList(albums));
			SvoeRadioPanel.instance().search_album_ddl.selectedIndex = 0;
		}
		
		public static function searchTracks(search_type:String, query:String, group_id:int, album_id:int):void 
		{
			var method:String,
				params:Object = new Object();
			
			switch (search_type) {
				case Strings.SEARCH_TYPE_ALL:
					method = METHOD_AUDIO_SEARCH;
					params.q = query;
					params.sort = 0;
					break;
				case Strings.SEARCH_TYPE_MY:
					method = METHOD_AUDIO_GET;
					break;
				case Strings.SEARCH_TYPE_GROUP:
					method = METHOD_AUDIO_GET;
					if (group_id <= 0) {
						Alert.show(Strings.SELECT_GROUP_FOR_SEARCH, Strings.ERROR);
						return;
					}
					params.owner_id = -group_id;
					if (album_id > 0)
						params.album_id = album_id;
					break;
			}
			loading();
			IOManager.search(method, params);
		}
		
		public static function setBroadcastStatus(status:String):void
		{
			SvoeRadioPanel.instance().broadcast_status = status;
		}
		
		public static function setBroadcastTrack(track_name:String):void
		{
			SvoeRadioPanel.instance().broadcast_track = track_name;
		}
		
		public static function setPlayingTrack(track:Object):void 
		{
			setBroadcastStatus(Strings.BUFFERING);
			setBroadcastTrack(track.name);
			
			if (SvoeRadioPanel.instance().queue_tracks_dp.length > 0)
				if (SvoeRadioPanel.instance().queue_tracks_dp.getItemAt(0).audio_id == track.audio_id)
					SvoeRadioPanel.instance().queue_tracks_dp.removeItemAt(0);
			
			hideLoading();
		}
		
		public static function firstLoad(api_result:Object):void 
		{
			loading();
			
			SvoeRadioPanel.instance().groups_dp.addAll(new ArrayList(api_result.groups));
			SvoeRadioPanel.instance().genres_dp.addAll(new ArrayList(api_result.genres));
			
			SvoeRadioPanel.instance().station_name = api_result.params.name;
			SvoeRadioPanel.instance().station_genre = api_result.params.genre_id;
			SvoeRadioPanel.instance().station_group = api_result.params.group_id;
			SvoeRadioPanel.instance().station_genre_ddl.selectedIndex = api_result.genre_index;
			SvoeRadioPanel.instance().station_group_ddl.selectedIndex = api_result.group_index;
			SvoeRadioPanel.instance().station_link = Config.STATION_LINK + SvoeRadioPanel.flashVars.group_id;
			SvoeRadioPanel.instance().generateStatsLimits(api_result.params.date);
			SvoeRadioPanel.instance().freezy = (api_result.params.freezy == 1);
			SvoeRadioPanel.instance().station_freezy_btn.label = SvoeRadioPanel.instance().freezy ? Strings.DEFREEZY_BTN : Strings.FREEZY_BTN;
			
			hideLoading();
		}
		
		public static function setQueueTracks(tracks:Array):void 
		{
			SvoeRadioPanel.instance().queue_tracks_dp.removeAll();
			if (tracks.length > 0) {
				SvoeRadioPanel.instance().queue_tracks_dp.addAll(new ArrayList(tracks));
				SvoeRadioPanel.instance().queue_save_btn.enabled = false;
			}
			hideLoading();
		}
		
		public static function setSearchTracks(tracks:Array):void 
		{
			SvoeRadioPanel.instance().search_tracks_dp.removeAll();
			SvoeRadioPanel.instance().search_tracks_dp.addAll(new ArrayList(tracks));
			
			hideLoading();
		}
		
		public static function getStats(year:int, month:int):void
		{
			IOManager.getStats(year, month);
		}
		
		public static function setStats(data:Array):void
		{
			SvoeRadioPanel.instance().setStats(data);
		}
		
		public static function freezyStation():void
		{
			IOManager.freezy(0);
		}
		
		public static function defreezyStation():void
		{
			IOManager.freezy()
		}
		
		public static function onErrorHandler(error:Object):void {
			var code:int = int(error.error_code);
			
			switch (code) {
				case 15:
					addStatusMessage(Strings.AUDIO_DISABLED);
					break;
				case ERROR_CODE_NOTICE:
				case ERROR_CODE_CRITICAL:
				case ERROR_CODE_API:
					addStatusMessage(error.error_msg);
					break;
				case ERROR_CODE_PROVIDER:
					addCriticalMessage(Strings.VK_ERROR);
					break;
				default:
					var msg:String = Strings.UNKNOWN_MSG_FORMAT;
					msg = msg.replace(Strings.REPLACE_DIGIT, code).replace(Strings.REPLACE_STRING, error.error_msg);
					addCriticalMessage(Strings.UNKNOWN_ERROR.replace(Strings.REPLACE_STRING, msg));
					break;
			}
		}
		
	}

}