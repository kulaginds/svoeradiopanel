package panel.misc 
{
	public class Strings 
	{
		/*
		 * Специальные строки.
		 */
		public static const EMPT_STR:String            = '';
		public static const UNKNOWN_MSG_FORMAT:String  = '"{code:%d,msg:%s}"';
		public static const SEARCH_TYPE_ALL:String     = 'all';
		public static const SEARCH_TYPE_MY:String      = 'my';
		public static const SEARCH_TYPE_GROUP:String   = 'group';
		public static const REPLACE_DIGIT:String       = '%d';
		public static const REPLACE_STRING:String      = '%s';
		public static const REMOVED_TRACK:String       = '[удаленный трек]';
		public static const ALL_ALBUMS:String          = 'Все альбомы';
		public static const ZERO:String                = '0';
		public static const COLON:String               = ':';
		public static const HYPHEN:String              = ' - ';
		public static const DOWN_SPACE:String          = '_';
		public static const COMMA:String               = ',';
		public static const FREEZY_BTN:String          = 'Заморозить станцию';
		public static const DEFREEZY_BTN:String        = 'Разморозить станцию';
		
		/*
		 * Сообщения.
		 */
		public static const PLAYING:String                  = 'Воспроизведение...';
		public static const STOPING:String                  = 'Воспроизведение остановлено.';
		public static const BUFFERING:String                = 'Буферизация...';
		public static const IN_AIR:String                   = 'В эфире:';
		public static const SELECT_STATION_IN_APP:String    = 'Выберите в основном приложении станцию для управления: Вкладка создать -> Мои станции.';
		public static const ERROR:String                    = 'Ошибка';
		public static const CRITICAL_ERROR:String           = 'Критическая ошибка';
		public static const NOTICE:String                   = 'Уведомление';
		public static const LOADING_WAIT_FOR:String         = 'Загрузка данных...\nПожалуйста, подождите.';
		public static const SELECT_GROUP_FOR_SEARCH:String  = 'Выберите группу для поиска';
		public static const SETTINGS_SAVED:String           = 'Настройки сохранены!';
		public static const QUEUE_SAVED:String              = 'Очередь сохранена!';
		public static const STATION_REMOVING:String         = 'Удаление станции';
		public static const READY_TO_REMOVE_STATION:String  = 'Вы действительно хотите удалить станцию?';
		
		/*
		 * Ошибки.
		 */
		
		public static const SERVER_ERROR:String      = 'Произошла ошибка сервера. Перезапустите приложение.';
		public static const VK_ERROR:String          = 'Произошла ошибка при запросе к ВКонтакте API. Перезапустите приложение или зайдите позже.';
		public static const UNKNOWN_ERROR:String     = 'Произошла неизвестная ошибка, сообщите её в группе. Данные: %s';
		public static const AUDIO_IOERROR:String     = 'Ошибка загрузки аудио.';
		public static const STATION_REMOVED:String   = 'Станция удалена!';
		public static const INCORRECT_STATS:String   = 'Выберите год и месяц для загрузки данных статистики.';
		public static const CONFIG_NOT_LOADED:String = 'Конфигурация не загружена.';
		
		/*
		 * Ошибки серверные или VK.
		 */
		public static const AUDIO_DISABLED:String                  = 'Аудиозаписи в группе отключены или доступ к ним ограничен.';
	}
}