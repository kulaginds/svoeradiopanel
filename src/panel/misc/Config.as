package panel.misc 
{
	/**
	 * @author Dmitry Kulagin <kulaginds.pp.ua>
	 */
	public class Config 
	{
		public static const APP_CONFIG_URL:String = '../appConfig.json';
		public static const SERVER_URL:String     = 'http://svoeradio/api/';//'http://svoeradio.azurewebsites.net/api/';
		public static const FIRST_VK_QUERY:String = 'return {"groups":API.groups.get({extended:1,filter:"editor"})};';
		public static const DEFAULT_VOLUME:Number = 0.5;
		public static const STATION_LINK:String   = 'http://vk.com/svoeradio?mid=-';
		public static const SEARCH_COUNT:int      = 200;
	}
}